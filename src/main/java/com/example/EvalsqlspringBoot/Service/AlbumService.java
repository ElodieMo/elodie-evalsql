package com.example.EvalsqlspringBoot.Service;

import com.example.EvalsqlspringBoot.Entity.Album;
import com.example.EvalsqlspringBoot.Repositry.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AlbumService {
    @Autowired
    private AlbumRepository albumRepository;

    public Iterable<Album> getAlbum() {
        return albumRepository.findAll();
    }

    public Album postAlbum(Album model) {
        return albumRepository.save(model);
    }


    public Optional<Album> getAlbumById(Long id) {
        return
                albumRepository.findById(id);
    }

    public Album save(Album model) {

        return albumRepository.save(model);
    }


}
