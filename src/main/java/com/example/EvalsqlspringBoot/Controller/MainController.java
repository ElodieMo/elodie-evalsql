package com.example.EvalsqlspringBoot.Controller;

import com.example.EvalsqlspringBoot.Entity.Album;
import com.example.EvalsqlspringBoot.Service.AlbumService;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;

@RestController
public class MainController {

    private final AlbumService albumService;

    public MainController(AlbumService albumService) {
        this.albumService = albumService;
    }

    @GetMapping("/albums")
    public Iterable<Album> getAlbum() {
        return albumService.getAlbum();
    }

    @GetMapping("/albums/{albumId}")
    public Optional<Album> getAlbumById(@PathVariable Long albumId) {
        return albumService.getAlbumById(albumId);
    }

    @PostMapping("/albums")
    public Album postAlbum(@RequestBody Album model) {
        return albumService.postAlbum(model);
    }

    @PutMapping("albums/{albumId}")
    public Album putAlbum(@RequestBody Album album, @PathVariable Long albumId) {
        Optional<Album> e = albumService.getAlbumById(albumId);

        if (e.isPresent()) {
            Album currentAlbum = e.get();

            String title = album.getTitle();

            if (title != null) {
                currentAlbum.setTitle(title);
            }
            String description = album.getDescription();
            if (description != null) {
                currentAlbum.setDescription(description);
            }
            String image = album.getImage();
            if (image != null) {
                currentAlbum.setImage(image);
            }
            Float duration = album.getDuration();
            if (duration != null) {
                currentAlbum.setDuration(duration);
            }
            Date release = album.getRelease();
            if (release != null) {
                currentAlbum.setRelease(release);
            }
            return albumService.save(currentAlbum);
        } else {
            return null;
        }
    }
}