package com.example.EvalsqlspringBoot.Repositry;

import com.example.EvalsqlspringBoot.Entity.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Long> {
}
