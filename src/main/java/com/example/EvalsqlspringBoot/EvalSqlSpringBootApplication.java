package com.example.EvalsqlspringBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvalSqlSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvalSqlSpringBootApplication.class, args);
	}

}
