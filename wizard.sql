1-Sélectionner le titre des catégories
SELECT title FROM `category`;


2-Sélectionner le nom et la puissance des potions dont la puissance est égale à 4
SELECT name AND power FROM `potion` WHERE power = 4;


3-Sélectionner le nom des ingrédients dont le nom commence par "Angel"
SELECT * FROM `ingredient` WHERE name LIKE 'Angel%';


4-Sélectionner le nom et la puissance des potions dont le nom contient le mot "Elixir" et la puissance est inférieure à 7
SELECT * FROM `potion` WHERE name LIKE '%Elixir%' AND power <7;


5-Sélectionner le nom des potions par ordre alphabétique
SELECT name FROM `potion` ORDER BY name;


6-Sélectionner le nom, l'effet et la puissance des 10 potions les plus puissantes
SELECT name, effect, power FROM `potion` ORDER by power limit 10;

