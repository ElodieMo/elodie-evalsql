Contexte du projet
Evaluation SQL et Spring Boot : fondamentaux

​

1. Dépôt Gitlab

​

Initialisez un projet Spring Boot, langage Java, avec Maven et les dépendances Spring Web, H2 Database et Spring Data JPA.

​

Créez un dépôt git contenant les fichiers du projet et liez-le à un dépôt GitHub/Gitlab dont le nom sera "prénom-evalsql" (bien-sûr, modifiez le prénom...). Ajoutez votre formateur⋅trice en collaborateur⋅trice du dépôt.

​

Travaillez en tenant compte des bonnes pratiques de git (faites des commits réguliers et correctement nommés) !

​

Vous ne devez cependant travailler que sur la branche main.

​

Vérifiez régulièrement que votre travail est bien présent sur GitLab !

​

2. Spring Boot

​

Vous devez réaliser une API pour un site de musique. Dans un premier temps, il vous est demandé de créer un CRUD pour la partie album : un album doit avoir un identifiant auto incrémenté, un titre (obligatoire, maximum 150 caractères), une description (optionnelle, au format texte), un lien vers une image (optionnel), la durée totale de l'album (obligatoire, par défaut 0), l'année de sortie de l'album (au format "YYYY-MM-DD", obligatoire).

​

Les noms des propriétés sont les suivantes (l'orthographe est importante !) :

    id
    title
    description
    image
    duration
    release

​

Les routes à créer sont les suivantes (l'orthographe est importante !) :

    GET /albums : récupérer la liste complète des albums
    GET /albums/{albumId} : récupérer un album par son identifiant
    POST /albums : créer un nouvel album
    PUT /albums/{albumId} : mettre à jour un album


3. Modélisation

​

Vous devez modéliser la base de données d'un site de vente de livres audio : ces derniers sont caractérisés par un ou plusieurs auteurs, un interprète, un titre, une durée et un prix. Un auteur a un nom, un prénom, une biographie et il peut avoir écrit plusieurs livres. Un interprète a un nom, un prénom et peut avoir prêté sa voix à plusieurs livres audio. Un livre audio peut être commandé par plusieurs utilisateurs. Un utilisateur possède une adresse email, un mot de passe et peut commander plusieurs livres audios : il ne peut cependant acheter qu'un seul livre par commande, et la date d'achat doit être enregistrée.

​

Réalisez le MCD (et uniquement le MCD) de l'énoncé précédent, puis partagez l'image de la modélisation sous le nom audiobook.jpg (ou .png).

4. Requêtes SQL

​

Importez dans MySQL le contenu du fichier wizards_n_potions.sql : https://gist.githubusercontent.com/bastienapp/95f2969796f4532c66768aec3cc12186/raw/805879d6aad0b5164108ac3c3d86b5b1c88d5fd4/wizards_n_potions.sql

​

Créez un fichier wizard.sql et réalisez les requêtes SQL suivantes (merci de copier-coller les énoncés avant chaque requête !) :

    Sélectionner le titre des catégories
    Sélectionner le nom et la puissance des potions dont la puissance est égale à 4
    Sélectionner le nom des ingrédients dont le nom commence par "Angel"
    Sélectionner le nom et la puissance des potions dont le nom contient le mot "Elixir" et la puissance est inférieure à 7
    Sélectionner le nom des potions par ordre alphabétique
    Sélectionner le nom, l'effet et la puissance des 10 potions les plus puissantes


Livrables

Un lien vers le dépôt contenant l'application Spring Boot
Un fichier audiobook.jpg avec le MCD
Un fichier wizard.sql avec les requêtes